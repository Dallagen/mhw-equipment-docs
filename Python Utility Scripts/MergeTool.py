import sys
settings = {}

prefer_l = lambda b,c,output: output.write(b)
prefer_r = lambda b,c,output: output.write(c)
case_case = lambda b,c,output: output.write(b if input("[L]eft or [R]ight?:").upper=="L" else c)

def query_end_preference(b,c,output):
    global settings
    response = input("End of merger files reached, do you want to [C]ontinue copying the base file or [E]nd it there?")
    if response.upper()=="E":
        return True
    else:
        settings["baseend"]=lambda _,__,___ : False
        return False        
        
def query_mistmatch_preference(b,c,output):
    global settings
    response = input("Both files have modifications, do you want to set a [D]efault resolution or do it [C]ase by Case?")
    if response.upper()=='D':
        response = input("Select a default: [L]eft or [R]ight")
        if response.upper()=="L":
            settings["mismatch"]=prefer_l
        else:
            settings["mismatch"]=prefer_r
    else:
        settings["mismatch"]=case_case
    
settings = {"baseend":query_end_preference, "mismatch":query_mistmatch_preference}

if __name__ == "__main__":
    if len(sys.argv)<5:
        print("Insufficient arguments: Base, Output, File1, File2")#, (FileN)*
        name = input("Press enter to quit.")
    else:
        base = open(sys.argv[1],'rb')
        output = open(sys.argv[2],'wb')
        merging_files = sys.argv[3:5]
        filelist = [open(filename, 'rb') for filename in merging_files]
        while True:
            a,b,c = base.read(1), filelist[0].read(1), filelist[1].read(1)
            test = (not a,not b,not c)
            #Early breaks when one all or all of merging files are done
            if all(test):
                break
            if test[1]:
                while c!='':
                    output.write(c)
                    c = filelist[1].read()
                break
            if test[2]:
                while c!='':
                    output.write(c)
                    c = filelist[1].read()
                break
            
            if test[0]:
                if settings["baseend"](b,c,output)   :
                    break
            else:
                if b==c:
                    output.write(b)
                elif b==a:
                    output.write(c)
                elif c==a:
                    output.write(b)
                else:
                    settings["mismatch"](b,c,output)
        base.close()
        output.close()
        (f.close() for f in filelist)