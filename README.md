# mhw-equipment-docs

Documentation on equipment file structures for the PC game Monster Hunter World.

[Wiki](https://gitlab.com/frederik-schumacher/mhw-equipment-docs/wikis/home)

## Credits

None of this would be possible without the efforts by members of the Monster Hunter World Modding Discord.

* MHVuze
* hexhexhex
* Fusion
